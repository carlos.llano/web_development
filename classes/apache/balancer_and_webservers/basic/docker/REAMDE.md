## Execute:
sudo docker-compose up -d

## To see stats:

192.168.0.33/haproxy?stats
admin/admin

# Stop and Delete:
sudo docker-compose down

## Images:
https://hub.docker.com/_/haproxy

https://hub.docker.com/_/httpd
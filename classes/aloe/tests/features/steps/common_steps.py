import aloe
import datetime
import http.client
from time import time
from nose.tools import assert_equals

############################
### Validate status code ###
############################

@aloe.step(u'I should get a \'(.*)\' response')
def i_should_get_response(step, expected_status_code):
    status = aloe.world.response[0]
    assert_equals(status, int(expected_status_code))

###################
### Get request ###
###################

@aloe.step(u'I send a request to \'(.*)\' to the endpoint \'(.*)\'')
def i_send_request(step, expected_http_verb, expected_endpoint):
    conn = http.client.HTTPConnection("localhost:7070")
    if expected_http_verb == 'GET':
        conn.request(expected_http_verb,
                     expected_endpoint,
                     headers={'Content-type': 'application/json'})
        res = conn.getresponse()
        data = res.read()
        aloe.world.response = [res.status, data, conn]
    elif expected_http_verb == 'POST':
        assert_equals(404, 404)
    elif expected_http_verb == 'PUT':
        assert_equals(404, 404)
    elif expected_http_verb == 'DELETE':
        assert_equals(404, 404)

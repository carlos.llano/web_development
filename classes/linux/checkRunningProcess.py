#!/usr/bin/python3
import subprocess
import os


# Validate if the process is running locally
number_process = int(subprocess.getoutput('ps aux | grep gimp | grep -v grep | wc -l'))

print(number_process)

if number_process == 0:
    print("gimp is down")
    pid = subprocess.Popen("gimp", stdout=subprocess.PIPE).pid 
else:
    print("all is ok")
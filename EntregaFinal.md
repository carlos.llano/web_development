Fecha de entrega: Noviembre 22 – 4pm – Sala Linux (La de siempre)

Entregables:
- Instalación automática de todos los servicios (movies, users, gateway, mysql, rabbitmq y servicio que escucha mensajes de otros servicios e inserta en base de datos)
- Documentación de los api usando swagger
- Todos los endpoints deben tener autenticación por medio del token jwt
- Debe existir un servicio que escucha mensajes de algún otro servicio e inserta esos mensajes en base de datos (es solo para llevar un log de eventos de algún endpoint que ustedes elijan).

Sobre la sustentación:
- Entra cada grupo, se elige cualquier persona al azar, muestra que no existen contenedores en la maquina donde van a hacer la sustentación, ejecutan el script de instalación y muestra la ejecución de varios endpoints, adicional muestra los mensajes del servicio de mensajería en bd.